nav:
  - Home: index.md
  - Getting Started: getting-started.md
  - Accounts:
      - Accounts : accounts/index.md
      - Passwords : accounts/passwords.md
      - Policy : accounts/policy.md
      - Collaboration Accounts : accounts/collaboration_accounts.md
  - Iris:
      - Users: iris/iris-for-users.md
      - PIs and Project Managers: iris/iris-for-pis.md
      - ERCAP and Iris Guide for Allocation Managers: iris/iris-for-allocation-managers.md
  - Systems:
      - Cori:
        - Cori System: systems/cori/index.md
        - Interconnect : systems/cori/interconnect/index.md
        - KNL Modes : systems/cori/knl_modes/index.md
        - Timeline:
          - Main Timeline: systems/cori/timeline/index.md
          - Default PE History: systems/cori/timeline/default_PE_history/PE_history.md
        - Application Porting and Performance:
          - Getting started on KNL: performance/knl/getting-started.md
      - Cori Large Memory Nodes:
        - Cori Large Memory Nodes: systems/cori-largemem/index.md
        - Running Large Memory Jobs: systems/cori-largemem/jobs.md
        - Cori Large Memory Software: systems/cori-largemem/software.md
      - Data Transfer Nodes : systems/dtn/index.md
  - Storage Systems:
      - Storage Systems: filesystems/index.md
      - Schematic: filesystems/schematic.md
      - Quotas and Purging: filesystems/quotas.md
      - Perlmutter scratch: filesystems/perlmutter-scratch.md
      - Cori scratch: filesystems/cori-scratch.md
      - Community: filesystems/community.md
      - Archive (HPSS):
        - Introduction: filesystems/archive.md
        - Storage Statistics: filesystems/archive-stats.md
      - Global Home: filesystems/global-home.md
      - Global Common: filesystems/global-common.md
      - Cori Burst Buffer: filesystems/cori-burst-buffer.md
      - Project: filesystems/project.md
      - Edison scratch: filesystems/edison-scratch.md
      - Backups: filesystems/backups.md
      - Unix File Permissions: filesystems/unix-file-permissions.md
  - Connecting:
      - Connecting to NERSC: connect/index.md
      - Multi-Factor Authentication: connect/mfa.md
      - NoMachine / NX, X Windows Accelerator: connect/nx.md
  - Environment:
     - User Environment: environment/index.md
     - Module Environment: environment/modules.md
  - Policies:
    - Login Node Policy: policies/login-nodes.md
    - Job Charging Policy: policies/charging-policy/index.md
    - Cray PE CDT Update Policy: policies/CDT-policy/index.md
    - Data Policy: policies/data-policy/policy.md
    - Software Support Policy:
      - Software Support Policy: policies/software-policy/index.md
      - Frequently Asked Questions: policies/software-policy/faq.md
      - Software Support List: policies/software-policy/software_state.md
      - Software Build Resources: policies/software-policy/build_resources.md
  - Development:
    - Compilers:
        - Native Compilers: development/compilers/native.md
        - Compiler Wrappers (recommended): development/compilers/wrappers.md
    - Build Tools:
        - Autoconf and Make: development/build-tools/autoconf-make.md
        - CMake: development/build-tools/cmake.md
        - Spack: development/build-tools/spack.md
    - Programming Models:
        - Programming Models: development/programming-models/index.md
        - MPI :
          - MPI: development/programming-models/mpi/index.md
          - Cray MPICH: development/programming-models/mpi/mpich.md
          - OpenMPI: development/programming-models/mpi/openmpi.md
          - Intel MPI: development/programming-models/mpi/intelmpi.md
        - OpenMP :
          - OpenMP: development/programming-models/openmp/openmp.md
          - OpenMP Resources : development/programming-models/openmp/openmp-resources.md
          - Tools for OpenMP : development/programming-models/openmp/openmp-tools.md
          - False Sharing Detection using VTune Amplifier : development/programming-models/openmp/sharing-vtune.md
        - UPC : development/programming-models/upc.md
        - UPC++ : development/programming-models/upcxx.md
        - Kokkos : development/programming-models/kokkos.md
        - Raja : development/programming-models/raja.md
        - Coarrays : development/programming-models/coarrays.md
    - Languages:
        - Julia: development/languages/julia.md
        - R: development/languages/r.md
        - IDL: development/languages/idl.md
        - Python:
            - Python: development/languages/python/overview.md
            - Using Python at NERSC: development/languages/python/nersc-python.md
            - Parallel Python: development/languages/python/parallel-python.md
            - Profiling Python: development/languages/python/profiling-debugging-python.md
            - Python on AMD CPUs: development/languages/python/python-amd.md
            - Porting Python to Perlmutter GPUs: development/languages/python/perlmutter-prep.md
            - FAQ and Troubleshooting: development/languages/python/faq-troubleshooting.md
    - Libraries:
        - Libraries: development/libraries/index.md
        - FFTW : development/libraries/fftw/index.md
        - LAPACK : development/libraries/lapack/index.md
        - MKL : development/libraries/mkl/index.md
        - LibSci : development/libraries/libsci/index.md
        - HDF5 : development/libraries/hdf5/index.md
        - NetCDF : development/libraries/netcdf/index.md
        - PETSc: development/libraries/petsc/index.md
    - Developer Tools:
        - Developer Tools: development/performance-debugging-tools/index.md
        - DDT: development/performance-debugging-tools/ddt.md
        - TotalView: development/performance-debugging-tools/totalview.md
        - GDB: development/performance-debugging-tools/gdb.md
        - HPCToolkit: development/performance-debugging-tools/hpctoolkit.md
        - STAT and ATP: development/performance-debugging-tools/stat_atp.md
        - Valgrind: development/performance-debugging-tools/valgrind.md
        - gdb4hpc and CCDB: development/performance-debugging-tools/gdb4hpc_ccdb.md
        - MAP: development/performance-debugging-tools/map.md
        - VTune: development/performance-debugging-tools/vtune.md
        - Advisor: development/performance-debugging-tools/advisor.md
        - Inspector: development/performance-debugging-tools/inspector.md
        - Reveal: development/performance-debugging-tools/reveal.md
        - APS: development/performance-debugging-tools/aps.md
        - Performance Reports: development/performance-debugging-tools/performancereports.md
        - CrayPat: development/performance-debugging-tools/craypat.md
        - Trace Analyzer and Collector: development/performance-debugging-tools/itac.md
        - LIKWID: development/performance-debugging-tools/likwid.md
        - Roofline Performance Model: development/performance-debugging-tools/roofline.md
        - Darshan: development/performance-debugging-tools/darshan.md
    - Containers:
        - Containers: development/shifter/overview.md
        - How to use Shifter: development/shifter/how-to-use.md
        - Common Issues: development/shifter/issues.md
        - Using Intel Compilers with Docker Images: development/shifter/intel.md
        - Examples: development/shifter/examples.md
  - Running Jobs:
    - Running Jobs: jobs/index.md
    - Queue Policy: jobs/policy.md
    - Examples: jobs/examples/index.md
    - Best Practices: jobs/best-practices.md
    - Common Errors: jobs/errors.md
    - Monitoring: jobs/monitoring.md
    - Affinity: jobs/affinity/index.md
    - Interactive: jobs/interactive.md
    - Reservations: jobs/reservations.md
    - Workflow Tools:
      - Workflow Tools: jobs/workflow-tools.md
      - Workflow nodes: jobs/workflow/workflow_nodes.md
      - GNU Parallel: jobs/workflow/gnuparallel.md
      - TaskFarmer: jobs/workflow/taskfarmer.md
      - FireWorks: jobs/workflow/fireworks.md
      - Nextflow: jobs/workflow/nextflow.md
      - Papermill: jobs/workflow/papermill.md
      - Parsl: jobs/workflow/parsl.md
      - Snakemake: jobs/workflow/snakemake.md
      - Other workflow tools: jobs/workflow/other_tools.md
    - Checkpoint/Restart:
      - Checkpoint/Restart: development/checkpoint-restart/index.md
      - DMTCP: development/checkpoint-restart/dmtcp/index.md
  - Applications:
    - Applications: applications/index.md
    - AMBER: applications/amber/index.md
    - Abinit: applications/abinit/index.md
    - BerkeleyGW: applications/berkeleygw/index.md
    - CP2K: applications/cp2k/index.md
    - CPMD: applications/cpmd/index.md
    - E4S:
      - E4S: applications/e4s/index.md
      - Spack-Gitlab-Pipeline: applications/e4s/spack_gitlab_pipeline.md
    - GAMESS: applications/gamess/index.md
    - Gromacs: applications/gromacs/index.md
    - LAMMPS: applications/lammps/index.md
    - Mathematica : applications/mathematica/index.md
    - MATLAB:
      - MATLAB: applications/matlab/index.md
      - MATLAB Compiler: applications/matlab/matlab_compiler.md
    - MOLPRO: applications/molpro/index.md
    - NAMD: applications/namd/index.md
    - NCAR Graphics: applications/ncargraphics/index.md
    - NWChem: applications/nwchem/index.md
    - SIESTA: applications/siesta/index.md
    - ParaView: applications/paraview/index.md
    - Q-Chem: applications/qchem/index.md
    - Quantum ESPRESSO: applications/quantum-espresso/index.md
    - VASP: applications/vasp/index.md
    - VisIt: applications/visit/index.md
    - WIEN2k: applications/wien2k/index.md
    - Wannier90: applications/wannier90/index.md
  - Analytics:
      - Analytics: analytics/analytics.md
      - Dask: analytics/dask.md
      - RStudio: services/rstudio.md
      - Spark: analytics/spark.md
  - Machine Learning:
      - Machine Learning: machinelearning/overview.md
      - TensorFlow: machinelearning/tensorflow.md
      - PyTorch: machinelearning/pytorch.md
      - TensorBoard: machinelearning/tensorboard.md
      - Benchmarks: machinelearning/benchmarks.md
      - Distributed training: machinelearning/distributed-training/index.md
      - Hyperparameter optimization: machinelearning/hpo.md
      - Science use-cases:
          Science use-cases: machinelearning/science-use-cases/index.md
          HEP CNN: machinelearning/science-use-cases/hep-cnn.md
  - Performance:
    - Perlmutter Readiness: performance/readiness.md
    - Getting started on KNL: performance/knl/getting-started.md
    - Vectorization : performance/vectorization.md
    - Parallelism : performance/parallelism.md
    - Memory Bandwidth: performance/mem_bw.md
    - Arithmetic Intensity: performance/arithmetic_intensity.md
    - Compiler Diagnostics: performance/compiler-diagnostics.md
    - I/O:
        - I/O: performance/io/index.md
        - I/O libraries: performance/io/library/index.md
        - Lustre: performance/io/lustre/index.md
        - Burst Buffer: performance/io/bb/index.md
        - KNL: performance/io/knl/index.md
    - Portability: performance/portability.md
    - Variability: performance/variability.md
    - Network: performance/network.md
    - Case Studies:
            - GPU Case Studies:
                - DESI: performance/case-studies/desi/index.md
                - C++ ML Interface: performance/case-studies/CPP_2_Py/cpptopy.md
                - MetaHipMer: performance/case-studies/metahipmer/index.md
                - Tomopy: performance/case-studies/tomopy/index.md
            - KNL Case Studies:
                - AMReX: performance/case-studies/amrex/index.md
                - BerkeleyGW: performance/case-studies/berkeleygw/index.md
                - Chombo-Crunch: performance/case-studies/chombo-crunch/index.md
                - EMGeo: performance/case-studies/emgeo/index.md
                - HMMER3: performance/case-studies/hmmer3/index.md
                - MFDn: performance/case-studies/mfdn/index.md
                - QPhiX: performance/case-studies/qphix/index.md
                - Quantum ESPRESSO: performance/case-studies/quantum-espresso/index.md
                - WARP: performance/case-studies/warp/index.md
                - XGC1: performance/case-studies/xgc1/index.md
    - KNL Cache Mode: performance/knl/cache-mode.md
  - Services:
      - Services: services/index.md
      - Spin:
          - Spin : services/spin/index.md
          - Rancher 1:
              - Getting Started with Rancher 1:
                  - Getting Started with Spin (Rancher 1): services/spin/rancher1/getting_started/index.md
                  - Spin (Rancher 1) Lesson 1: services/spin/rancher1/getting_started/lesson-1.md
                  - Spin (Rancher 1) Lesson 2: services/spin/rancher1/getting_started/lesson-2.md
                  - Spin (Rancher 1) Lesson 3: services/spin/rancher1/getting_started/lesson-3.md
              - Cheat Sheet : services/spin/rancher1/cheatsheet.md
              - Reference Guide: services/spin/rancher1/reference_guide.md
          - Rancher 2:
              - Getting Started with Rancher 2 : services/spin/rancher2/getting_started.md
              - Migration Guide : services/spin/rancher2/migration.md
              - Concepts and Terminology : services/spin/rancher2/concepts.md
              - CLI Guide : services/spin/rancher2/cli.md
      - Science Gateways : services/science-gateways.md
      - NEWT : services/newt.md
      - Jupyter : services/jupyter.md
      - Globus : services/globus.md
      - CDash :
        - CDash : services/cdash/index.md
        - CTest/CDash with CMake : services/cdash/with_cmake.md
        - CTest/CDash without CMake : services/cdash/without_cmake.md
      - CVMFS : services/cvmfs.md
      - Databases: services/databases.md
      - GridFTP: services/gridftp.md
      - Scp: services/scp.md
      - Bbcp: services/bbcp.md
  - Science Partners:
    - Joint Genome Institute (JGI):
      - JGI Resources: science-partners/jgi/index.md
      - Systems:
        - JGI Systems Overview: science-partners/jgi/systems.md
        - Cori Genepool: science-partners/jgi/cori-genepool.md
        - Cori ExVivo: science-partners/jgi/cori-exvivo.md
      - JGI Filesystems: science-partners/jgi/filesystems.md
      - JGI Software: science-partners/jgi/software.md
      - JGI Training and Tutorials: science-partners/jgi/training.md
      - JGI Databases and Web Services: science-partners/jgi/services.md
    - Best Practices for Experimental Science: science-partners/bestpractices-eod.md
  - Acronyms: help/acronyms.md
  - Current Known Issues: current.md
  - Help: help/index.md

# Project Information
site_name: NERSC Documentation
site_description: NERSC Documentation
site_author: NERSC
site_dir: public
site_url: "https://docs.nersc.gov/"
repo_name: GitLab/NERSC/docs
repo_url: https://gitlab.com/NERSC/nersc.gitlab.io
edit_uri: blob/main/docs/

# Configuration
strict: true

theme:
  name: material
  custom_dir: nersc-theme
  favicon: assets/images/favicon.ico
  logo: assets/images/logo.png

extra_javascript:
  - javascripts/extra.js
  - https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML
  - https://app-script.monsido.com/v2/monsido-script.js
extra_css:
  - stylesheets/extra.css

extra:
  social:
    - icon: material/home-circle
      link: https://www.nersc.gov
    - icon: material/help-circle
      link: https://help.nersc.gov
    - icon: material/heart-pulse
      link: https://www.nersc.gov/live-status/motd/
    - icon: fontawesome/brands/github
      link: https://github.com/NERSC
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/NERSC
    - icon: fontawesome/brands/slack
      link: https://www.nersc.gov/users/NUG/nersc-users-slack/

# Extensions
markdown_extensions:
  - meta
  - attr_list
  - footnotes
  - admonition
  - codehilite:
      guess_lang: false
  - toc:
      permalink: true
  - pymdownx.arithmatex
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.emoji:
      emoji_generator: !!python/name:pymdownx.emoji.to_svg
  - pymdownx.inlinehilite
  - pymdownx.magiclink
  - pymdownx.mark
  - pymdownx.smartsymbols:
      fractions: false
  - pymdownx.superfences
  - pymdownx.details
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  - pymdownx.snippets

google_analytics:
  - 'UA-115581982-1'
  - 'auto'

plugins:
  - search:
      prebuild_index: python
  - minify:
      minify_html: true
