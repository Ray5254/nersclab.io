# RStudio

RStudio provides popular open source and enterprise-ready professional software for the R statistical computing environment.
NERSC has an RStudio instance at [rstudio.nersc.gov](https://rstudio.nersc.gov).

!!! warning "R Studio users are encouraged to switch to Jupyter"
    NERSC strongly encourages users to use Jupyter ([jupyter.nersc.gov](https://jupyter.nersc.gov)) for their R
    analysis.  Jupyter offers better support and resources and
    it is easier for users to create custom environments with
    Anaconda.

    For details on Jupyter, see the [Jupyter Docs](../jupyter).

    For details on using Anaconda to create R environments, 
    see the
    [R docs](../../programming/high-level-environments/r/#creating-r-environments-with-anaconda).
