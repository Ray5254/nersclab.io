# Perlmutter Readiness

!!! warning
    This page is currently under active development. Check
    back soon for more content.

This page contains recommendations for application developers and
users to "hit the ground running" on
[Perlmutter](https://www.nersc.gov/systems/perlmutter/) which unlike
previous NERSC systems features GPUs (NVIDIA A100).

Testing of performance on relevant hardware and compatibility with the
software environment are both important.

## Getting Started with GPUs

### Introduction to GPUs

### Additional Resources

* [Ampere in depth](https://devblogs.nvidia.com/nvidia-ampere-architecture-in-depth/)

### Slides / Video from training events

### Training Events

## Prebuilt applications

## High-level considerations for using GPUs

Unlike CPUs, accelerator program requires additional care with memory
management. Transfers from Host-to-Device and Device-to-Host are
comparatively much slower than the memory bandwidth and care should be
taken to minimize them.

### Tensor cores

The ["Ampere"
architecture](https://www.nvidia.com/en-us/data-center/nvidia-ampere-gpu-architecture/)
on the NVIDIA A100 GPUs on Perlmutter feature [tensor
cores](https://developer.nvidia.com/tensor-cores), which are specialized
accelerators which can significantly accelerate certain types of computational
kernels, typically those involving operations on matrices. An innovation of the
tensor cores on A100 GPUs which may be of interest to HPC applications running
at NERSC is support for double precision floating point (FP64) data types, in
contrast to previous generations of tensor cores which were limited to reduced
precision data types. A second improvement in A100 tensor cores is support for
and acceleration of operations on [sparse data
structures](https://blogs.nvidia.com/blog/2020/05/14/sparsity-ai-inference/).

NVIDIA provides substantial documentation regarding programming models which
can target the A100 tensor cores:

  - [overview of tensor cores on NVIDIA GPUs](https://www.nvidia.com/en-us/data-center/tensor-cores/)
  - [deep learning framework examples which use tensor cores](https://developer.nvidia.com/deep-learning-examples)
  - [NVIDIA NGC containers with tensor core examples](https://ngc.nvidia.com/catalog/resources?orderBy=scoreDESC&pageNumber=0&query=tensor%20core&quickFilter=&filters=)
  - [video series with mixed precision examples using tensor cores](https://developer.nvidia.com/blog/video-mixed-precision-techniques-tensor-cores-deep-learning/?ncid=so-twi-dplgdrd3-73821)
  - [Ampere tuning guide for tensor cores](https://docs.nvidia.com/cuda/ampere-tuning-guide/index.html#tensor-operations)
  - [warp matrix functions in CUDA C++](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#wmma)

## Programming Models

The choice of programming model depends on multiple factors including:
number of performance critical kernels, source language, existing
programming model, and portability of algorithm. A 20K line C++ code
with 5 main kernels will have different priorities and choices vs a
10M line Fortran code.

NERSC supports a wide variety of programming models on Perlmutter:

### Native CUDA C/C++

CUDA is NVIDIA's native GPU programming language. CUDA is
well established, mature and its design is the basis and backend for most
other languages and frameworks, such as HIP and Kokkos.  NVIDIA works
directly with the scientific community to develop CUDA's performance and
features and has a variety of teaching material and community outreach
to help users achieve their computational goals.  NVIDIA also develops a
variety of performant mathematical, parallel algorithm, communication and
other libraries to help users achieve optimal performance on NVIDIA hardware.

Given CUDA's ubiquitous nature, it may be the best language to study and
follow in order to understand GPU programming. Its highly developed programming
model is the standard for discussion throughout the community.  Many other
programming models are compatible with CUDA.  This can allow highly
optimized CUDA code to be implemented in critical code paths while using more
portable frameworks elsewhere.

CUDA is a good option for smaller codes, codes that are primiarily looking
to run on NVIDIA systems and codes that continuously develop and maximize
their performance. It will often have the newest features and high
performance on NVIDIA systems and writing in CUDA will get you the newest
features, performance improvements and bug fixes quickly.

However, CUDA only runs on NVIDIA hardware, so writing in CUDA will require
additional work to achieve portability to other vendor's GPUs. That process
may be simple, such as using a tool to convert, or quite complex if specialized,
performant algorithms have been created. CUDA also typically requires more
tuning and understanding than pragma-based languages, such as OpenMP or
portability layers, such as Kokkos, so developers looking for a quick port may
wish to explore other options.

CUDA:

* [CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit)
* [CUB](https://github.com/NVIDIA/cub/) (now included in the CUDA toolkit)
* [CUDA-X Libraries](https://developer.nvidia.com/gpu-accelerated-libraries)

Documentation:

* [CUDA Documentation](https://docs.nvidia.com/cuda/index.html)
* [CUDA C++ Programming Guide](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html)
* [CUDA Dev Blog for HPC]( https://developer.nvidia.com/blog/category/hpc/)

Training:

* [CUDA Training Series](https://www.olcf.ornl.gov/cuda-training-series/) (under "presentation" at the bottom of each page)
* [An Even Easier Introduction to CUDA](https://developer.nvidia.com/blog/even-easier-introduction-cuda/)

Codes at NERSC that use CUDA:

* [AMReX](https://amrex-codes.github.io)
* [MetaHipMer2] (https://bitbucket.org/berkeleylab/mhm2/src/master/)

!!! tip
	With C++ it is possible to encapsulate such code through
	template specialization in order to preserve portability.

### Directives

NVIDIA compilers are recommended for obtaining good performance with
directive based approaches.

#### OpenMP

#### OpenACC

OpenACC is a programming framework that allows users to expose parallelism in their code via compiler directives.
Compilers that support the framework  will interpret the directives and schedule the parallel code accordingly.
On Perlmutter, NVHPC/HPC-SDK (formerly PGI), GCC and CCE compilers will support OpenACC directives where, the degree of the 
support will vary based on the compiler. Here is the OpenACC specification [OpenACC spec](https://www.openacc.org/specification).

The benefit of OpenACC is its ease of use. But the downside of the approach is the inability to perform low level optimizations. 
Following are few of the highlights and recommendations for OpenACC:

* OpenACC provides `kernels` construct for users to identify code segments that may contain parallelism. It allows users to rely
on compilers to identify and safely parallelize the marked code sections. This is a good starting place for OpenACC.
* Take advantage of OpenACC clauses such as `gang`, `vector` and `worker` to map hierarchical parallelism onto the GPUs.
* `collapse` clause can be used to expose more parallelism in nested loop structures.
* Optimize the data movement in applications. Due to high cost of data transfer between CPU and GPU memories, it is beneficial to 
perform computation on the GPU even when the code lacks parallelism if the trade-off is a reduction in memory transfers. 
* Reorder loops and data structures to take advantage of memory coalescing. 
* OpenACC allows interoperability with other programming languages via the use of clauses such as `host_data` and `deviceptr`.
* OpenACC implementation of NVHPC compiler allows interoperability with CUDA. This implies that kernels that need low level
optimizations via architecture specific programming can be intermixed with parts of code that are implemented in OpenACC. 
* Some of the advanced OpenACC features include asynchronus operations and multi-device programming.

The following link provides a detailed description of the best practices to be followed in OpenACC [OpenACC best practices](https://www.openacc.org/sites/default/files/inline-files/OpenACC_Programming_Guide_0.pdf).

### C++ language based

#### ISO C++

!!! Example
	* Parallel STL (pSTL) to achieve parallelism in many STL functions
          * [Intel's implementation](https://software.intel.com/content/www/us/en/develop/articles/get-started-with-parallel-stl.html)
          * [NVIDIA/PGI's roadmap for GPU acceleration](https://developer.download.nvidia.com/video/gputechconf/gtc/2019/presentation/s9770-c++17-parallel-algorithms-for-nvidia-gpus-with-pgi-c++.pdf)
	* [Executors](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p0443r12.html) for heterogeneous programming and code execution
	* [mdspan](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p0009r6.html) for multi-dimensional arrays
          * [Kokkos implementation](https://github.com/kokkos/mdspan)

#### Kokkos

Kokkos is a C++ based programming model that allows mapping abstract hierarchies of parallelism onto diverse architectures.
It provides a unified front-end for users to write portable parallel code via its constructs while the framework provides 
architecture specific backends to map the constructs onto the intended hardware. 
A few of the framework's highlights are :

* The `View` abstraction for multidimensional arrays. It allows users to chose the optimal order to traverse the data structures 
based on the loop ordering and target architecture via its semantics. 
* The framework allows users to expose additional parallelism by taking advantage of its `MDRange` constructs. 
* Kokkos also allows the users to exploit multiple levels of parallelism and take advantage of hierarchical parallelism available in
most of the modern high performance computers. 

Kokkos framework is availiable at [Github](https://github.com/kokkos).
Here is a link to the tutorial lecture series on use of the Kokkos framework [kokkos lecture series](https://github.com/kokkos/kokkos-tutorials/wiki/Kokkos-Lecture-Series).

#### SYCL

[SYCL / DPC++](https://github.com/intel/llvm/tree/sycl)

#### HIP

HIP is AMD's GPU portability layer.  HIP is compiled with either a CUDA
or ROCm backend to run on NVIDIA or AMD GPUs, respectively.  ROCm is
AMD's native programming language, although its a lower-end language
than HIP and it is expected that users write their codes in HIP and
rarely use ROCm directly.

HIP is designed to directly mimic CUDA.  HIP uses an extremely similar
programming model and it designed so that most of its calls are direct
copies of cuda, in most cases simply replacing "cuda" with "hip".  HIP
uses a subset of CUDA calls (roughly based on a target version of CUDA)
and additional AMD specific features, so porting between HIP and CUDA
is typically a very simple process.

HIP is a good option for those code teams with knowledge of the CUDA
programming model and who also want to achieve maximum performance on
AMD systems.  For teams targeting both Perlmutter and Frontier (NVIDIA
and AMD architechtures), HIP may be the ideal programming model.

However, HIP may not work well for the same codes that should avoid CUDA:
codes that want full portability, minimial tuning, minimal editing or
don't have the time to maintain the code base.  OpenMP, Kokkos, or another
abstraction layer is likely the better bet for such coding efforts.

HIP:

* [HIP](https://github.com/ROCm-Developer-Tools/HIP)
* [ROCm Libraries](https://rocmdocs.amd.com/en/latest/ROCm_Libraries/ROCm_Libraries.html)
* [HIPify](https://github.com/ROCm-Developer-Tools/HIPIFY)

Documentation:

* [HIP Documentation](https://rocmdocs.amd.com/en/latest/Programming_Guides/Programming-Guides.html)

Codes at NERSC that use CUDA:

* [AMReX](https://amrex-codes.github.io)

#### Raja

#### Thrust

[thrust](https://docs.nvidia.com/cuda/thrust/index.html)

### Fortran

The Fortran standard avoids language and features which target particular
architectures. This generic approach provides Fortran compilers the freedom to
interpret features in different ways, including how to offload kernels to GPUs.
Consequently, different compilers may implement certain features of the Fortran
language differently in terms of how they are accelerated on GPUs.

On Perlmutter, perhaps the most effective resource for accelerating Fortran
code on GPUs is the compiler included in the [NVIDIA HPC
SDK](https://developer.nvidia.com/hpc-sdk). The HPC SDK provides three separate
mechanisms by which Fortran programs may be accelerated on A100 GPUs. These are
described below.

#### GPU acceleration with standard Fortran

The HPC SDK enables GPU acceleration of several different Fortran features. One
is [`DO CONCURRENT`
loops](https://developer.nvidia.com/blog/accelerating-fortran-do-concurrent-with-gpus-and-the-nvidia-hpc-sdk/)
which can be offloaded to GPUs by adding the `-stdpar` compiler flag. The HPC
SDK can also offload several other Fortran features to GPUs by mapping them to
[cuTENSOR](https://docs.nvidia.com/cuda/cutensor/index.html) functions; these
features include
[`RESHAPE`](https://docs.nvidia.com/hpc-sdk/compilers/fortran-cuda-interfaces/index.html#cftensor-ctex-reshape),
[`TRANSPOSE`](https://docs.nvidia.com/hpc-sdk/compilers/fortran-cuda-interfaces/index.html#cftensor-ctex-transpose),
[`SPREAD`](https://docs.nvidia.com/hpc-sdk/compilers/fortran-cuda-interfaces/index.html#cftensor-ctex-spread),
[certain element-wise
expressions](https://docs.nvidia.com/hpc-sdk/compilers/fortran-cuda-interfaces/index.html#cftensor-ctex-expressions),
[`MATMUL`](https://docs.nvidia.com/hpc-sdk/compilers/fortran-cuda-interfaces/index.html#cftensor-ctex-matmul),
and [several element-wise
functions](https://docs.nvidia.com/hpc-sdk/compilers/fortran-cuda-interfaces/index.html#cftensor-ctex-functions).

!!! Note "Please provide feedback to NERSC"
         GPU acceleration of standard Fortran intrinsics as described above is a new
         feature in the NVIDIA HPC SDK compiler. Users are encouraged to experiment with
         this feature in the compiler and provide feedback to NERSC by filing a ticket
         at the online [help desk](https://help.nersc.gov).

#### OpenMP

The HPC SDK also implements a subset of [OpenMP offload
capabilities](https://docs.nvidia.com/hpc-sdk/compilers/hpc-compilers-user-guide/index.html#openmp-use)
which can be used in Fortran programs too. Features of OpenMP which are not
implemented in the HPC SDK are parsed but ignored in order to maintain
portability across OpenMP-enabled Fortran compilers.

A feature in OpenMP implemented in the HPC SDK which may be of special interest
to Fortran programmers is the
[`loop`](https://www.openmp.org/spec-html/5.1/openmpsu51.html#x76-840002.11.7)
construct, which is similar to the [`kernels` construct in
OpenACC](https://www.openacc.org/sites/default/files/inline-images/Specification/OpenACC-3.1-final.pdf).
The `loop` construct is a descriptive directive which allows the compiler the
freedom to analyze the contained loop for dependencies and apply optimizations
for GPU offload as it sees fit; see
[here](https://docs.nvidia.com/hpc-sdk/compilers/hpc-compilers-user-guide/index.html#openmp-loop)
for details regarding its implementation in the HPC SDK.

#### OpenACC

The HPC SDK continues to support [OpenACC](https://www.openacc.org) for GPU
acceleration; see
[here](https://docs.nvidia.com/hpc-sdk/compilers/hpc-compilers-user-guide/index.html#acc-use)
for details.

### Julia

### Python

For more information about preparing Python code for Perlmutter
GPUs, please see this [page](../development/languages/python/perlmutter-prep.md).

### MPI

Some modern MPI implementations support a feature known as "CUDA-awareness."
This term remains somewhat loosely defined, but is generally understood to mean
that pointers to data residing in GPU device memory can be used as arguments in
MPI function calls invoked on the CPU.

CUDA-awareness in MPI has several potential benefits but also some potential
drawbacks. One significant benefit is that it can be a convenience to the
programmer. For example, when executing an MPI function call, a CUDA-aware MPI
implementation may inspect the pointer to determine whether the data it points
to resides in CPU or GPU memory, and if the latter, the MPI implementation may
execute a `cudaMemcpy()` to copy the data from the GPU to the CPU, and then
execute the MPI function on the CPU memory. Such an implementation would save
the programmer the work of writing the `cudaMemcpy()` in their own code, but
would not offer any performance benefit. An alternative implementation of this
behavior might be for CUDA to allocate the memory using `cudaMallocManaged()`,
in which case the CUDA runtime itself may copy the data back to CPU memory
before executing the MPI function.

There are also potential performance benefits to CUDA-aware implementations of
MPI, namely that the library can take advantage of hardware features like
[GPUDirect](https://developer.nvidia.com/gpudirect), such that if the user
executes an MPI function with a pointer to data in device memory, the MPI
library may copy the data directly from GPU device memory to the network device
on the node, without copying the data to CPU memory. This behavior would
provide both a convenience and a performance benefit.

However, there are also potential drawbacks to writing programs which target
CUDA-aware MPI implementations. Chiefly, CUDA-aware MPI is not standardized,
and so its behavior may vary among different MPI implementations which each
claim to be "CUDA-aware." For example, one MPI implementation may support
CUDA-awareness only in point-to-point communication routines, whereas another
may support it only in collective routines. This may result in programs
executing correctly on one system, but crashing on another.

Details regarding CUDA-awareness support in MPI on Perlmutter will be
forthcoming. However, some open-source MPI implementations already support this
behavior, e.g., [OpenMPI](https://www.open-mpi.org/faq/?category=runcuda) and
[MVAPICH2-GDR](http://mvapich.cse.ohio-state.edu/userguide/gdr/). Users are
encouraged to review those pages to understand how CUDA-awareness is
implemented in those libraries.

## Machine Learning Applications

We support distributed PyTorch and TensorFlow on Perlmutter.
Currently, we recommend to use the relevant Shifter images
which are based on optimized NVIDIA NGC ones, details can be
found in our Machine Learning
[documentation](../machinelearning/overview.md#deep-learning-frameworks).

### Performance and IO optimization

Deep learning pipelines often need performance tuning to make
best use of powerful accelerators. Special considerations
should be given to IO bottlenecks. Our NERSC-NVIDIA
[Deep Learning at Scale Tutorial (Supercomputing 2020)](https://github.com/NERSC/sc20-dl-tutorial/)
provides guidance on how to analyze and address some of these performance challenges.

### Case studies

1. [Optimizing Data Processing Pipelines 3D CNN (Cosmo-3D)](https://drive.google.com/file/d/1-mjnYqF2nRD7JEaQ6SWEIGHDkyj3YWq4/view)

## Libraries

### Alternatives to Intel Math Kernel Library (MKL) for CPU math functions

Several generations of NERSC systems have provided the [Intel Math Kernel
Library
(MKL)](https://software.intel.com/content/www/us/en/develop/tools/oneapi/components/onemkl.html),
which provides a large, diverse collection of functions and routines for
commonly used numerical calculations, which are summarized
[here](https://software.intel.com/content/www/us/en/develop/documentation/onemkl-linux-developer-guide/top.html).
MKL is optimized for Intel architectures, such as the Haswell and Knights
Landing processors on the [Cori](../../systems/cori) system.

While it is possible that Intel MKL may work on the AMD CPUs on Perlmutter, it
is not guaranteed, nor is it certain that if it does work that will achieve
similar levels of performance that its users have enjoyed on Intel
architectures. Consequently, NERSC users are encouraged to explore other
software offerings which may fulfill many of the same roles as MKL.

Currently, the [AMD AOCL](https://developer.amd.com/amd-aocl/) library provides
implementations of several math functions and routines provided in MKL,
including [BLAS](https://developer.amd.com/amd-aocl/blas-library/),
[LAPACK](https://developer.amd.com/amd-aocl/blas-library/#libflame),
[ScaLAPACK](https://developer.amd.com/amd-aocl/scalapack/), [sparse linear
algebra](https://developer.amd.com/amd-aocl/aocl-sparse/),
[FFTW](https://developer.amd.com/amd-aocl/fftw/), [optimized math
functions](https://developer.amd.com/amd-aocl/amd-math-library-libm/) and
[random number generators](https://developer.amd.com/amd-aocl/rng-library/).

!!! Note "GPU implementations of math libraries are often much faster"
    While CPU-based implementations of math functions and routines included in
    MKL are available and provide below, users should first consider GPU
    implementations, which are often much faster.

### Math libraries for Nvidia GPUs

One way to port existing applications to GPUs without rewriting the entire application in a GPU-aware programming model is to use
GPU-accelerated libraries to execute performance-critical calculations.
Below is an incomplete list of GPU-accelerated math libraries which are expected to run effectively on Perlmutter.

#### Linear algebra

##### Dense linear algebra

###### BLAS/LAPACK/ScaLAPACK

- BLAS: Nvidia [cuBLAS](https://developer.nvidia.com/cublas)
- LAPACK: Nvidia [cuSOLVER](https://developer.nvidia.com/cusolver) (not all LAPACK subroutines have been implemented)
- BLAS/LAPACK: [MAGMA](https://icl.utk.edu/magma) (not all LAPACK subroutines have been implemented)
- ScaLAPACK replacement: [SLATE](https://icl.utk.edu/slate)

###### Eigensolvers

- single GPU:

    - [cuSOLVER](https://developer.nvidia.com/cusolver)
    - [MAGMA](https://icl.utk.edu/magma)
    
- distributed (multi-node):

    - [ELPA](https://elpa.mpcdf.mpg.de)
    - [SLATE](https://icl.utk.edu/slate)

##### Sparse linear algebra

###### Matrix operations/solvers

- single GPU:

    - [cuSPARSE](https://developer.nvidia.com/cusparse): Nvidia CUDA sparse matrix library
    - [Ginkgo](https://ginkgo-project.github.io)
    - [STRUMPACK](https:github.com/pghysels/STRUMPACK)

- distributed (multi-node):

    - [SuperLU](https://portal.nersc.gov/project/sparse/superlu)
    - [PETSc](https://www.mcs.anl.gov/petsc)
    - [Trilinos](https://trilinos.github.io)

###### Eigensolvers

- distributed (multi-node):

    - [SLEPc](https://slepc.upv.es)
    - [Trilinos](https://trilinos.github.io)

###### Algebraic multigrid

- [AmgX](https://developer.nvidia.com/amgx)
- [hypre](http://www.llnl.gov/CASC/hypre)
- [Trilinos](https://trilinos.github.io)
- [PETSc](https://www.mcs.anl.gov/petsc)

#### Solver libraries

- [PETSc](https://www.mcs.anl.gov/petsc)
- [Trilinos](https://trilinos.github.io)

#### FFT

- Single GPU:

    - Nvidia [cuFFT](https://developer.nvidia.com/cufft)/cuFFTW (cuFFTW library provides the FFTW3 API to facilitate porting of
    existing FFTW applications), supports multiple GPUs on a single compute node

- Distributed FFT (multi-node):

    - [FFTX](http://www.spiral.net/software/fftx.html)
    - [heFFTe](http://icl.utk.edu/fft)

### Ray tracing

- Nvidia [OptiX](https://developer.nvidia.com/optix)

## IO Considerations

All file systems can be access from Perlmutter except Cori Scratch. Details about the Perlmutter Scratch File System can be found [here](../filesystems/perlmutter-scratch.md).

## AMD CPU Considerations

## Tools

## Running Applications

## General recommendations

!!! fail "Write a validation test"
    Performance doesn't matter if you get the wrong answer!

    * https://doi.org/10.1086/342267
    * https://doi.org/10.1109/MCSE.2017.3971169

* Define benchmarks for performance. These should represent the
  science cases you want to run on Perlmutter.
* Use optimized libraries when possible (FFT, BLAS, etc).
* Start with 1 MPI rank per GPU.
* Start with UVM and add explicit data movement control as needed.
* Minimize data movement (Host to device and device to host transfers).
* Avoid device allocations (Use
  a [pool allocator](https://en.wikipedia.org/wiki/Memory_pool))

### Algorithms

The ability for applications to achieve both portability and high
performance across computer architectures remains an open
challenge.

However there are some general trends in current and emerging HPC
hardware: increased thread parallelism; wider vector units; and deep,
complex, memory hierarchies.

In some cases a [performance portable](portability.md) algorithm can
realized by considering generic "wide vectors" which could map to
either GPU SIMT threads or CPU SIMD lanes.

## Case Studies and Examples

Many examples of kernels and applications being ported to NVIDIA GPUs are
available in literature, conference proceedings, and online at GitHub, GitLab,
etc. Below is a small selection of examples and case studies which may be
useful to NERSC users as they port their applications to GPUs.

### NERSC proxy app suite

NERSC maintains a collection of [proxy
applications](https://gitlab.com/NERSC/nersc-proxies/info), which are typically
small codes with only a handful of computationally intensive algorithms
included. These proxy apps are often written in multiple languages and
programming models, in order to evaluate performance of those languages and
models, as well as to illustrate how one might port an existing code (e.g., one
which does not offload anything to a GPU) to a language or model which enables
efficient GPU acceleration.

## References and Events

* P3HPC Workshop at SC
* [2019 DOE Performance Portability](https://doep3meeting2019.lbl.gov)
* https://performanceportability.org
