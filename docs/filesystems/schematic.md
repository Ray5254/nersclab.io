# File System Schematics

## Cori Scratch

![Cori Scratch](cori_scratch.png)

### Abbreviations

|||
|:--:|-------------------------|
| CMP| Chip Multicore Processor|
| OSS|Object Storage Server: a component of a Lustre File System|
| OST|Object Storage Target: a component of a Lustre File System|
| LNET|Lustre network router|
| MDS| Metadata Server, manage file operation, e.g., create new file, write to shared file|

## HPSS Archive

![HPSS Archive](HPSS_Archive_Architecture_20201207.png)
