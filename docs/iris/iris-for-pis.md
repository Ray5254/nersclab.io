# Iris Guide for PIs and Project Managers

## NERSC Allocations

Allocations are made to project accounts (also called 'allocation
accounts').  Every year Principal Investigators (PIs) or their
designated PI Proxies apply for NERSC resources for both existing
and new project accounts. See the [ERCAP Allocation Request
Form](https://www.nersc.gov/users/accounts/allocations/request-form/).

### Project Roles

With the advent of Iris, we introduce two additional project
management roles, Project Resource Managers and Project Membership
Managers, for finer-grained management, which we hope can benefit
large projects where some tasks performed by PIs or PI proxies can
now be delegated.  PIs, PI Proxies, Project Resource Managers and
Project Membership Managers, collectively called "project managers,"
can work together in managing users and computer resources for their
projects, using the [Iris web interface](https://iris.nersc.gov).
It is entirely up to PIs, however, to have all, some or none of the
PI Proxy, Project Resource Manager, and Project Membership Manager
roles created for their projects.

The table below summarizes permissions for project-related roles
in Iris:

| | PI | PI Proxy | Project Resource Manager | Project Membership Manager | User |
| ----------- |:---:|:---:|:---:|:---:|:---:|
| Accept Users                                                           | X | X | - | - | - |
| Change Project Role                                                    | X | X | - | - | - |
| Edit Allocation (allotting NERSC hours, enabling the premium QOS, ...) | X | X | X | - | - |
| Edit Group (create a Unix group)                                       | X | X | X | X | - |
| Edit HPSS Allocs                                                       | X | X | X | - | - |
| Edit Members (e.g., adding a member to a Unix group)                   | X | X | - | X | - |
| Edit Organizations                                                     | X | - | - | - | - |
| Edit Project Details                                                   | X | X | - | - | - |
| Edit Quotas ([Community File System](../filesystems/community.md))     | X | X | - | - | - |
| Invite User                                                            | X | X | - | X | - |
| Mark Continuing Users                                                  | X | X | - | X | - |
| View Project                                                           | X | X | X | X | X |

---

## Navigating Around Iris

This section is to provide quick information on the top level Iris
menus only.  More detailed information on how to navigate or use
various utilities in Iris can be found in the [Iris Guide for
Users](iris-for-users.md).

### Menu Bar

The menu bar at the top of the page has several menus, allowing you
to access different parts of Iris.

![Iris menu bars](images/iris_elvis_menubars.png)
{: align="center" style="border:1px solid black"}

-   The **Iris** icon in the top left corner is clickable and serves
    as the Home button. When clicked, it brings you back to the
    "Homepage" which displays your compute allocation usage summary
    info.

-   The **Projects** pull-down lists the projects that you have
    access to and allows you to see usage info for each.

-   The **Reports** pull-down provides various usage report options.

-   The **search box** allows you to quickly get the information
    you need about individual users or projects.

-   The account pull-down under your account name in the top right
    corner contains menu for displaying what your Iris roles are
    (e.g., User, PI, ...) and what permissions you have in Iris.
    Someone in your project group whose role is the PI, a PI proxy
    or a Project Resource Manager can edit in Iris how much compute
    time a user in the group is allowed to use.

    To logout from Iris, select the **Logout** menu item.

For certain menu items, there is also a sub menu bar under the top
menu bar, consisting of tabs for different menu items. When this
bar appears, the first value on the left refers to the context for
the page displayed. For example, if the sub menu is about your
account itself (that is, when the Iris icon is selected), the label
will be your full name, as shown in the above figure. If the sub
menu is about an allocation account, it will display the allocation
account name.

These menu bars are always available to you, and do not scroll when
you scroll down the screen.

### Sorting the Display Data

Displayed data can be sorted by a column by clicking on the column
heading.  By default data is sorted in the ascending order. Clicking
on the heading again, you can toggle the sorting order.

The column selected for data sorting has its column heading marked
as chosen.

---

## Check Account Usage

### Check a User's Account Usage

To look up a user's account usage:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Find the row for the user.

Alternatively,

1.  Enter the username in the search box at the top right corner
    of the Iris webpage.
2.  Select the user from the search results.
3.  Click the 'CPU' tab.

### View NERSC Hour Charges and Usage by Users

To view the NERSC hour charges by users in an allocation account:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar. Note that the 'CPU' tab is selected by default.
2.  Usage by users is provided in the 'Per-User compute allocations'
    table beneath the YTD (Year-to-date) usage plot.

### View HPSS Charges and Usage by Users

To view the HPSS charges by users:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Storage' tab.
3.  Usage by users is provided in the 'HPSS Archive' table.

## Change a User's NERSC Hour Quota

To change the user quota (the percentage or the number of NERSC
Hours of a project's allocation that a user is allowed to use):

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar. Note that the 'CPU' tab is selected by default.
2.  Usage by users is provided in the 'Per-User compute allocations'
    table beneath the YTD (Year-to-date) usage plot.
3.  Adjust the numbers in the boxes in either the 'Allocated Hours'
    box for a specific number of NERSC hours, or the 'Allocation %
    of Project' box for a percentage that the user is allowed to
    use. The 'Allocated Hours' field is only valid for MPP allocations.
    If both values are set, the percentage value will be used.
[comment]: # (These two fields are mutually)
[comment]: # (exclusive; when you enter a value on the 'Allocated Hours')
[comment]: # (field, the 'Allocation % of Project' field will be disabled so)
[comment]: # (it cannot be modified during the same update action.)
4.  Click the 'Update Allocations' button above the table.)

## Enabling the premium QOS (available from AY21)

Enabling the ['premium' QOS](../jobs/policy.md#premium) for project
users is a new feature starting in AY21. Project managers with the
proper permission can enable the premium QOS for continuing users
now, but the effect will be for AY21.

To enable a user to run jobs in the 'premium':

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Click the 'CPU' tab and go to the 'Per-User compute allocations'
    section.
3.  Click the pencil icon in the 'QOS' column for the user you want
    to give the premium QOS. This will show a pop-up menu.
4.  Select 'premium', and click OK. A black box labeled as 'premium'
    will show up in the QOS column.
5.  Click the 'Update Allocations' button above.
    top menu bar.

## Adjust a Directory's Quota in the Community File System

To change a directory's [CFS (Community File
System)](../filesystems/community.md) quota:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Storage' tab.
3.  Usage by CFS directories is provided in the 'CFS Directory
    Usage' table beneath the YTD (Year-to-date) CFS Storage plot.
4.  Adjust the 'Directory Storage Quota (TB)' value and/or
    the 'Directory File Quota' value for the CFS directory whose
    space and/or inode quota you want to modify. Note that the sum
    of the 'Directory Storage Quota' column must be equal to or
    less than the 'Storage Allocation' value for the project, shown
    at the top of the page. Similary, the sum of the 'Directory
    File Quota' column must be equal to or less than the 'Files
    Allocation' value.
5.  Click the 'Update Quotas' button above the table.

## Add a CFS Directory to Your Project

To add a new or existing [CFS (Community File
System)](../filesystems/community.md) directory to your project:

1.  Select the project from the 'Projects' pull-down menu on the top
menu bar.
2.  Select the 'Storage' tab.
3.  Usage by CFS directories is provided in the 'CFS Directory
    Usage' table beneath the YTD (Year-to-date) CFS Storage plot.
4.  Click the '+ New' button above the table.
5.  Fill in the Directory name. This can be a new name or an existing
directory you previously controlled.
6.  Choose a Unix Group. This can either be an existing Unix Group or
a new Unix Group with the same name as the new directory.
7.  Enter the Owner username.
8.  Enter the number in the 'Directory Storage Quota (TB)' and/or the 'Directory File Quota'.
9.  Click the 'Save Changes' button.

## Change a User's HPSS Quota

To change the user HPSS quota:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Storage' tab.
3.  Usage by users is provided in the 'HPSS Archive' table at the
    bottom. Adjust the number in the '% Allowed' box for a user.
4.  Click the ‘Update HPSS Allocations’ button above the table.

## Add a User to a Project

### For a User with an Active NERSC Account

To add an existing user who has an active NERSC account:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  Click the '+ Add User' button.
4.  Type the user's name, username or email in the 'Select a user'
    box.
5.  Wait until Iris returns a list of the users matching the entered
    text. Select the user from the list.
6.  In the dialog box that appears:
    -   Set the project role for the user:
	`user`, `project_resource_manager`, `project_membership_manager`,
	or `pi_proxy`.
        - NOTE: There can be only one user with the PI role per project.
    -   Set a NERSC hour quota in either the 'Allocated Hours' or
        '% of Project Allocated' box. If both are set, the percentage
	value will be used. Set the HPSS quota for the user in the
	'% of HPSS Storage' box.
    -   Click the 'Save Changes.'
7.  Once you enter the data for the user's account, it will added to your project
    and the user will be sent an email notification.

### For a New User to NERSC or an Existing User with a Deactivated Account

NERSC provides an online form that will allow new users of NERSC
to submit a request for a new account. The same form can be used
by existing users with a deactivated account to make a request for
reactivating the account.  The form can be found at:
[https://iris.nersc.gov/add-user](https://iris.nersc.gov/add-user).
New users should click on the "I need a new NERSC account" button.
Existing users with Deactivated accounts should click on the "I have
a current NERSC account" button.

Once the user has submitted a request and their account has completed 
the compliance vetting, PIs and PI Proxies (that is, the project managers
with the 'Accept Users' permission) will be notified about the
submission.  Any of them may then log into Iris and approve the
account request, following the steps below.

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  You will see the pending requests in the 'Pending Members'
    table (you may need to scroll down the page).  Review the information 
    provided by the user to be sure they are a valid user who needs an 
    account under your project.
4.  If everything looks OK, click on the <span style="color:green">**green**</span> Thumbs Up button.
    In the dialog box that appears:
    -   Set the project role for the user: `user`,
        `project_resource_manager`, `project_membership_manager`,
	or `pi_proxy`.
        - NOTE: There can be only one user with the PI role per project.
    -   Set a NERSC hour quota in either the 'Allocated Hours' or
        '% of Project Allocated' box. If both are set, the percentage
	value will be used. Set the HPSS quota for the user in the
	'% of HPSS Storage' box.
    -   Click the 'Save Changes.'
5.  If you do not know this person, you can click on the <span style="color:red">**red**</span> Thumbs Down
    button.
6.  After you have approved the account request, the approved user
    will receive a Welcome email and an additional containing a link that 
    will allow the user to set their initial password.

## Delete a User from a Project

To delete a user from a project:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  Find the row for the user you wish to delete, and click the
    row.
4.  Click the '- Remove User' button.

Keep in mind that if a user is a member of more than one project,
removing them from only your project will allow their account to
remain active in order to access the other project(s). If they are a
member of only your project, when you remove them, their account will
start a process of disabling their access allowing them time to move
or archive files. 

!!! tip
	Please see NERSC's [data retention policy](../policies/data-policy/policy.md).

## Change a User's Project Role

To change a user's role in a project:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  Find the row for the user you wish to delete. In the Role column,
    change the user's role to one of `user`,
    `project_resource_manager`, `project_membership_manager`,
    `pi_proxy`, and `pi`.
4.  Click the 'Update All Roles' button.

## Update a User's Organization Information

Please ask your users to keep their contact information up to date
because project managers cannot update the information in Iris for
other members.

But PIs can update organization information for a member if there
is a change (of course, the user can update the information by
himself/herself). To update the information:

1.  Enter the user name or username in the search box in the top
    right corner.
2.  Select the user from the search results.
3.  Note that the 'Profile' tab is selected for the user account.
4.  Start to type the correct organization name in the 'Select
    an organization' field in the 'Self-service User Info' section.
    Select the correct one from the returned list of instutions
    with the matching words.
5.  Enter the new email address for the user in the 'Primary Email'
    field. The email address should have the correct domain name
    for the institution.
6.  Click the 'Update' button.

## Add a User to a Unix Group

To add a user to a Unix group:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Groups' tab.
3.  Choose a desired Unix group from the box on the left.
4.  Type the user's name, username or email in the 'Add a user'
    box.
5.  Click the '+ Add User' button. Check the user appears in the
    member table for the group.

## Delete a User from a Unix Group

To delete a user from a Unix group:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Groups' tab.
3.  Choose a desired Unix group from the box on the left.
4.  Check the box next to the user that you want to remove from the
    group.
5.  Click the '- Remove Users' button. Check the user is removed
    from the member table for the group.

## Transfer Resources

[//]: # (To transfer resources from a repository you administer to another repository, select Transfer Resources from the Actions menu.)

This feature will be available soon.

## Set Your User List for the Next Allocation Year

This interface is available for PIs, PI-proxies and Project Membership
Managers, only in the month or so before the new allocation year
starts. To review and set user statuses for the next allocation
year:

1.  Select the project from the 'Projects' pull-down menu on the
    top menu bar.
2.  Select the 'Roles' tab.
3.  Keep the checkbox in the 'Cont. User?' column checked only for
    the continuing users.
4.  Check the box in the 'Allow Premium?' column for the users that
    can run in the premium QOS.
5.  Click the 'Update All' button.

If you want to mark all users in a project as continuing users,
simply click the 'Toggle All Cont. Users' text on the right.

Similarly, you can select or deselect all users for the premium
QOS, by simply clicking the 'Toggle All Allow Premium' text on the
right.

Please keep in mind that for security reasons only active users
should keep their accounts.
