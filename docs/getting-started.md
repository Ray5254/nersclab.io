# Welcome to NERSC

Welcome to the National Energy Research Scientific Computing Center
(NERSC)!

!!! success "About this page"
	This document will guide you through the basics of using NERSC's
	supercomputers, storage systems, and services.

## What is NERSC?

[NERSC](https://www.nersc.gov) provides High Performance Computing and
Storage facilities and support for research sponsored by, and of
interest to, the U.S.  Department of Energy (DOES) Office of Science
(SC). NERSC has the unique programmatic role of supporting all six
Office of Science program offices: Advanced Scientific Computing
Research, Basic Energy Sciences, Biological and Environmental
Research, Fusion Energy Sciences, High Energy Physics, and Nuclear
Physics.

Scientists who have been awarded research funding by any of the
offices are eligible to apply for an allocation of NERSC
time. Additional awards may be given to non-DOE funded project teams
whose research is aligned with the Office of Science's
mission. Allocations of time and storage are made by DOE.

NERSC is a national center, organizationally part of [Lawrence
Berkeley National Laboratory](https://www.lbl.gov) in Berkeley, CA.
NERSC staff and facilities are primarily located at Berkeley Lab's
Shyh Wang Hall on the Berkeley Lab campus.

## Computing & Storage Resources

### Cori

Cori is a [Cray XC40](https://www.cray.com/products/computing/xc-series)
supercomputer with approximately 12000 compute nodes.

* [Detailed system specifications](./systems/cori/index.md)
* [Live status](https://www.nersc.gov/live-status/motd/)

### Community File System (CFS)

The [Community File System (CFS)](./filesystems/community.md) is a
global file system available on all NERSC computational systems. It
allows sharing of data between users, systems, and the "outside
world".

* [Detailed usage information](./filesystems/community.md)
* [Live status](https://www.nersc.gov/live-status/motd/)

### HPSS (High Performance Storage System) Archival Storage

The High Performance Storage System (HPSS) is a modern, flexible,
performance-oriented mass storage system. It has been used at NERSC
for archival storage since 1998. HPSS is intended for long term
storage of data that is not frequently accessed.

* [Detailed usage information](./filesystems/archive.md)
* [Live status](https://www.nersc.gov/live-status/motd/)

## NERSC Accounts

In order to use the NERSC facilities, you need:

1. Access to an allocation of computational or storage resources as a
   member of a project
2. A user account with an associated user login name (also called
   username).

* [Obtaining an account](./accounts/index.md#obtaining-an-account)
* [NERSC Allocations](https://www.nersc.gov/users/accounts/allocations/)
* [Iris](https://iris.nersc.gov): Account and allocation management web interface
* [Password rules](./accounts/passwords.md)

!!! tip "With [Iris](https://iris.nersc.gov) you can"
	* check allocation balances
	* change passwords
	* run reports
	* update contact information
	* clear login failures
	* change login shell
	* and more!

## Connecting to NERSC

!!! check "MFA is *required* for NERSC users"

* [Multi-Factor Authentication (MFA)](./connect/mfa.md)
* [SSH](./connect/index.md#ssh)
* [Cori login nodes](./connect/index.md)
* [Troubleshooting connection problems](./connect/index.md#troubleshooting)
* [Live status](https://www.nersc.gov/live-status/motd/)

## NERSC Users Group (NUG)

Join the [NERSC Users Group](https://www.nersc.gov/users/NUG/): an
independent organization of users of NERSC resources.

!!! tip
	NUG maintains a Slack workspace that all users are welcome to
	[join](https://www.nersc.gov/users/NUG/nersc-users-slack/).

## Software

NERSC and its vendors supply a rich set of HPC utilities,
applications, and programming libraries. 

* [NERSC Supported Software Status List](./policies/software-policy/software_state.md).
* Application specific documentation on this site.
* Available [modules](./environment/index.md#nersc-modules-environment)
  login to Cori and run `module avail`)

!!! question "Something missing?"
	If there is something missing that you would like to have on
	our systems, please [submit a request](https://help.nersc.gov) and
	we will evaluate it for appropriateness, cost, effort, and benefit
	to the community.

## Computing Environment

!!! info
	[`$HOME` directories](./filesystems/global-home.md) are shared
	across all NERSC systems (except HPSS)

* [Customizing your environment](./environment/index.md)

## Compiling/ building software

* [compilers at NERSC](./development/compilers/wrappers.md)
* [environment](./environment/index.md)

## Running Jobs

Typical usage of the system involves submitting scripts (also
referred to as "jobs") to a batch system such as
[Slurm](https://slurm.schedmd.com/).

* [overview of jobs at NERSC](./jobs/index.md)
* [rich set of examples](./jobs/examples/index.md)

## Interactive Computing

NERSC also supports interactive computing.

* [Interactive jobs](./jobs/interactive.md)
* [Jupyter](./services/jupyter.md)

## Data Sharing 

### Security and Data Integrity

Sharing data with other users must be done carefully. Permissions
should be set to the minimum necessary to achieve the desired
access. For instance, consider carefully whether it's really necessary
before sharing write permissions on data. Be sure to have archived
backups of any critical shared data. It is also important to ensure
that private login secrets (like SSH private keys or apache
htaccess files) are not shared with other users (either intentionally
or accidentally). Good practice is to keep things like this in a
separate directory that is as locked down as possible.

### Sharing with Other Members of Your Project

NERSC's [Community file system](filesystems/community.md) is set up
with group read and write permissions and is ideal for sharing with
other members of your project. There is a directory for every active
project at NERSC and all members of that project should have access to
it by default.

### Sharing with NERSC Users Outside of Your Project

You can share files and directories with NERSC users outside of your
project by adjusting the unix file permissions. We have an extensive
write up of unix file permissions and how they work
[here](filesystems/unix-file-permissions.md).

NERSC provides two commands: `give` and `take` which are useful for
sharing small amounts of data between users.

To send a file or path to `<receiving_username>`:

```
nersc$ give -u <receiving_username> <file or directory>
```

To receive a file sent by `<sending_username>`:

```
nersc$ take -u <sending_username> <filename>
```

To take all files from `<sending_username>`:

```
nersc$ take -a -u <sending_username>
```

To see what files `<sending_username>` has sent to you:

```
nersc$ take -u <sending_username>
```

For a full list of options pass the `--help` flag.

!!! warning
    Files that remain untaken 12 weeks after being given will be
    purged from the staging area.
    
### Sharing Data outside of NERSC

You can easily and quickly share data over the web using our [Science
Gateways](services/science-gateways.md) framework.

You can also share large volumes of data externally by setting up a
[Globus Sharing Endpoint](services/globus.md#globus-sharing).

## Data Transfers

NERSC partners with ESNet to provide a high speed connection to the
outside world. NERSC also provides several tools and systems optimized
for data transfer.

### External Data Transfer

!!! tip
    **NERSC recommends transferring data to and from
    NERSC using [Globus](services/globus.md)**

[Globus](services/globus.md) is a web-based service that solves
many of the challenges encountered moving data between systems. Globus
provides the most comprehensive, efficient, and easy to use
service for most NERSC users.

However, there are other tools available to transfer data between
NERSC and other sites:

* [scp](services/scp.md): standard Linux utilities suitable for smaller files (<1GB)
* [GridFTP](services/gridftp.md): parallel transfer software for large files

### Transferring Data Within NERSC

!!! tip
    **"Do you need to transfer at all?"  If your data is on NERSC
    Global File Systems (`/global/cfs`, `/global/projecta`,
    `/global/cscratch`), data transfer may not be necessary because
    these file systems are mounted on almost all NERSC
    systems. However, if you are doing a lot of IO with these files,
    you will benefit from staging them on the most performant file
    system. Usually that's the local scratch file system or the Burst
    Buffer.**

* Use the the unix command `cp`, `tar` or `rsync` to copy files within
   the same computational system. For large amounts of data use Globus
   to leverage the automatic retry functionality

### Data Transfer Nodes

The [Data Transfer Nodes (DTNs)](systems/dtn/index.md) are servers
dedicated for data transfer based upon the ESnet Science DMZ
model. DTNs are tuned to transfer data efficiently, optimized for
bandwidth and have direct access to most of the NERSC file
systems. These transfer nodes are configured within Globus as managed
endpoints available to all NERSC users.

### NERSC FTP Upload Service

NERSC maintains [an FTP upload
service](https://www.nersc.gov/users/storage-and-file-systems/nersc-ftp-upload-service/)
designed for external collaborators to be able to send data to NERSC
staff and users.

## Getting Help

NERSC places a very strong emphasis on enabling science and providing
user-oriented systems and services.

### Documentation

NERSC maintains extensive [documentation](https://docs.nersc.gov). 

!!! check "NERSC welcomes your contributions"
    These pages are hosted from a
    [git repository](https://gitlab.com/NERSC/nersc.gitlab.io) and
    [contributions](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md)
    are welcome!

    [Fork this repo](https://gitlab.com/NERSC/nersc.gitlab.io/-/forks/new)

### New User Training Materials

The NERSC New User Training covers the basics on our computational systems; accounts and allocations;
programming environment, tools, best practices; and data ecosystem.
[Slides](https://www.nersc.gov/users/training/events/new-user-training-june-16-2020/) 
and [Recordings](https://www.youtube.com/playlist?list=PL20S5EeApOSsFDfNrshVdiaQKZK4CNFq7) 
are good references for the specific topics.

### Account support

* [Online Help Desk](https://help.nersc.gov) (preferred, login required)
* [email](mailto:accounts@nersc.gov) (if you cannot login)

!!! info "Availability"
	Account support is available 8-5 Pacific Time on business days.

### Consulting

NERSC's consultants are HPC experts and can answer just about all of
your technical questions.

* [Online Help Desk](https://help.nersc.gov)

!!! info "Availability"
	Account support is available 8-5 Pacific Time on business days.

### Operations

!!! warning "For *critical* system issues only."

* Please check the [Online Status Page](https://www.nersc.gov/live-status/motd/) 
before calling 1-800-666-3772 (USA only) or 1-510-486-8600, Option 1
